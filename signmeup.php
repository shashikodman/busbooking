<?php  
	include('signup.php');

	if(isset($_SESSION['login_user1'])) {
		header("location: index.php");
	}

	include('header.php');
?>

<div class="row">
	<div class="col s6 offset-s3 z-depth-1" id="loginbox">
		<form method="post" action="">
			<div class="row" style="border-bottom: 1px solid black;">
				<h4 class="center-align">Customer Registration</h4>
			</div>
			<div class="row valign-wrapper">
				<div class="circle  grey lighten-1 valign z-depth-2">
					<p class="center-align">
						<i class="material-icons large white-text">perm_identity</i>
					</p>
				</div>
			</div>
			<div class="row input-field">
				<input type="text" name="name" id="name">
				<label for="name">Full name</label>
			</div>
			<div class="row input-field">
				<input type="email" name="email" id="email">
				<label for="email">Email</label>
			</div>
			<div class="row input-field">
				<input type="text" name="mobile" id="mobile">
				<label for="mobile">Mobile Number</label>
			</div>
			<div class="row input-field">
				<input type="text" name="username" id="username">
				<label for="username">Username</label>
			</div>
			<div class="row input-field">
				<input type="password" name="password" id="password">
				<label for="password1">Password</label>
			</div>
			<div class="row">
				<button type="submit" name="submit" class="btn waves-effect col s12 black">Register</button>
			</div>
			<span class="red-text">
				<?php echo $error; ?>
			</span>
		</form>
	</div>
</div>

<?php  
	include('footer.php');
?>