<?php  
	include('session1.php');
	include('header.php');
?>

<div class="row aboutus">
	<div class="container aboutuscontent z-depth-2">
		<h4 class="blue-text text-darken-2">About Us</h4>
		<hr>
		<p class="grey-text text-darken-3">
			Bus Booking is an inter-city ticketing service in India. It connects 
			various cities across India with just a click of a button.
		</p>
		<p class="grey-text text-darken-3">
			Bus Booking provides travellers, the most uncomplicated and hassle-free 
			booking experience ever.
		</p>
		<p class="grey-text text-darken-3">
			Choose your destination, select bus of your choice, view the seat layout,
			choose convenient seats, and book your ticket in just a few clicks!
		</p>
		<br><br>
		<p class="grey-text text-darken-3 center-align" style="font-size: 25px;">
			Enjoy using Bus Booking!
		</p>
	</div>
</div>

<?php  
	include('footer.php');
?>