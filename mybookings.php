<?php  
	include('session1.php');
	
	include('header.php');
	
	$del = 0;
	if(isset($_GET["bookId"])){
	  $query = "delete FROM reserved WHERE reserveid=".$_GET["bookId"];
	  $result = mysqli_query($con, $query) ;
	  $del=1;
	  }
	  
	echo "
    <div class=\"row white z-depth-1\" id=\"profileheader\">
      <h5 class=\"blue-text\" id=\"welcome\">
        Welcome : &nbsp; $login_session1
      </h5>
    </div>";
?>
<div class="row grey lighten-3" style="padding: 20px 0;">
	<div class="container">
		<h5 style="font-size: 25px; margin-bottom: 30px;">My Bookings</h5>
	
	<?php
	  if($del == 1){
	      echo "<div class=\"row\"><div class=\"col s6 m6\">Bus cancelled successfully..</div></div> ";
	  }
	?>
	</div>
	<div class="row">

	<?php

		$query = "SELECT * FROM bus B, passenger P, reserved R, trip T 
				  WHERE B.busid=R.busid
				  AND T.tripid=R.tripid
				  AND P.passengerid=R.passengerid
				  AND P.username='".$_SESSION['login_user1']."'";
		$result = mysqli_query($con, $query)
					or die("Error querying database: ".mysqli_error($con));	
		if(mysqli_num_rows($result) > 0) {	
			while ($row = mysqli_fetch_array($result)) {

				$from = $row['source'];
				$to = $row['destination'];
				$date = $row['date'];
				$busname = $row['busname'];
				$busid = $row['busid'];
				$passengername = $row['name'];
				$seat = $row['seat'];
				$arrival = $row['arrival'];
				$departure = $row['departure'];
				$reserveid = $row['reserveid'];
				$boardingpoint = $row['boardingpoint'];
				$droppingpoint = $row['droppingpoint'];
			
				echo "
				<div class=\"container white z-depth-1 hoverable tickets\">
					<div class=\"row blue white-text\" style=\"margin-bottom: 0px; padding: 10px 0; margin-top: -10px; border-top-radius: 10px;\">
						<div class=\"col s12 m6\">
							$from --> $to
						</div>
						<div class=\"col s12 m3\">
							$date
						  </div>
						  <div class=\"col s12 m3\">
							<a class=\"btn btn-danger\"href=\"mybookings.php?bookId=$reserveid\">Cancel</a>
						  </div>
					</div>
					<div class=\"row\" style=\"margin-bottom: 0px; margin-top: 10px;\">
						<div class=\"col s12 m6\">
							<span class=\"blue-text\">Bus Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</span> $busname
						</div>
						<div class=\"col s12 m6\">
							<span class=\"blue-text\">Bus ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</span> $busid
						</div>
					</div>
					<div class=\"row\" style=\"margin-bottom: 0px;\">
						<div class=\"col s12 m6\">
							<span class=\"blue-text\">Passenger Name&nbsp;:</span> $passengername
						</div>
						<div class=\"col s12 m6\">
							<span class=\"blue-text\">Seat Number&nbsp;&nbsp;&nbsp;:</span> $seat
						</div>
					</div>
					<div class=\"row\" style=\"margin-bottom: 0px;\">
						<div class=\"col s12 m6\">
							<span class=\"blue-text\">Departure&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</span> $departure
						</div>
						<div class=\"col s12 m6\">
							<span class=\"blue-text\">Arrival&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</span> $arrival
						</div>
					</div>
					<div class=\"row\" style=\"margin-bottom: 0px;\">
						<div class=\"col s12 m6\">
							<span class=\"blue-text\">Boarding&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</span> $boardingpoint
						</div>
						<div class=\"col s12 m6\">
							<span class=\"blue-text\">Dropping&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</span> $droppingpoint
						</div>
					</div>
					<div class=\"row\" style=\"margin-bottom: 0px; margin-top: 10px; border-top: 1px solid #e0e0e0; padding-top: 7px;\">
						<div class=\"col s12 m12\">
							<span class=\"grey-text\">Ticket Number&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</span> $reserveid
						</div>
					</div>
				</div>";
			}	
		}
		else {
			echo "
			<div class=\"container white z-depth-1 tickets\">
				<p>
				No tickets found. Click <a href=\"index.php\">here</a> to book one.
				</p>
			</div>";
		}	
	?>	
	</div>
</div>

<?php  
	include('footer.php');
?>
