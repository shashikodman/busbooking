<?php  
	include('login1.php');

	if(isset($_SESSION['login_user1'])) {
		header("location: index.php");
	}

	include('header.php');
?>

<div class="row">
	<div class="col s12 m6 l4 offset-m3 offset-l4 z-depth-1" id="loginbox">
		<form method="post" action="">
			<div class="row" style="border-bottom: 1px solid black;">
				<h4 class="center-align">Customer Login</h4>
			</div>
			<div class="row valign-wrapper">
				<div class="circle  grey lighten-1 valign z-depth-2">
					<p class="center-align">
						<i class="material-icons large white-text">perm_identity</i>
					</p>
				</div>
			</div>
			<div class="row input-field">
				<input type="text" name="username1" id="username1">
				<label for="username1">Username</label>
			</div>
			<div class="row input-field">
				<input type="password" name="password1" id="password1">
				<label for="password1">Password</label>
			</div>
			<div class="row">
				<button type="submit" name="submit1" class="btn waves-effect col s12 black">Login</button>
			</div>
			<div class="row">
				No account? <a href="signmeup.php">Register</a> Now!
			</div>
			<span class="red-text">
				<?php echo $error; ?>
			</span>
		</form>
	</div>
</div>

<?php  
	include('footer.php');
?>