<?php 
	
	include('session1.php');

	
	if(isset($_SESSION['passengerid']) && isset($_SESSION['busid']) && isset($_SESSION['tripid']) && isset($_SESSION['price'])) {

		include('header.php');
		include('db.php');
		
		$passengerid = $_SESSION['passengerid'];
		$busid = $_SESSION['busid'];
		$tripid = $_SESSION['tripid'];
		$price = $_SESSION['price'];
		$boardingpoint = $_SESSION['boardingpoint'];
		$droppingpoint = $_SESSION['droppingpoint'];

		foreach ($_SESSION['selectedseat'] as $seat) {

			$query2 = "SELECT * FROM reserved WHERE tripid='$tripid' AND busid='$busid' AND passengerid='$passengerid' AND seat='$seat'";
			$result2 = mysqli_query($con, $query2)
				or die("Error querying database: ".mysqli_error($con));
			if(mysqli_num_rows($result2) > 0) 
				header("location: index.php");	

			$query1 = "INSERT INTO reserved (tripid, busid, passengerid, boardingpoint, droppingpoint, seat, price)
					   VALUES('$tripid', '$busid', '$passengerid', '$boardingpoint', '$droppingpoint', '$seat', '$price')";
			$result1 = mysqli_query($con, $query1)
				or die("Error querying database: ".mysqli_error($con));	 

			unset($_SESSION['tripid']);
			unset($_SESSION['busid']);
			unset($_SESSION['selectedseat']);
			unset($_SESSION['price']);	

			
		}		
		mysqli_close($con);
	}
	else
		header("location: index.php");
		  
?>

<div class="row" style="padding: 50px;">
	<h4>You have successfully booked your ticket!</h4>
	<br><br>
	<a href="mybookings.php" class="btn black waves-effect">View Tickets</a>
</div>	

<?php  
	include('footer.php');
?>