<?php  
	
	include('session1.php');

	if(isset($_SESSION['totalprice'])) {
?>

<!DOCTYPE html>
<html>
<head>
	<title>Processing Transaction</title>
	<link rel="stylesheet" type="text/css" href="css/materialize.css">
</head>
<body>
<div class="center-align" style="margin-top: 70px;">
	<h4>Processing Transaction</h4>
	<p>Please wait...</p>
</div>
<div style="width: 50px; margin-left: auto; margin-right: auto;">
	<div class="preloader-wrapper big active">
	      <div class="spinner-layer spinner-blue">
	        <div class="circle-clipper left">
	          <div class="circle"></div>
	        </div><div class="gap-patch">
	          <div class="circle"></div>
	        </div><div class="circle-clipper right">
	          <div class="circle"></div>
	        </div>
	      </div>

	      <div class="spinner-layer spinner-red">
	        <div class="circle-clipper left">
	          <div class="circle"></div>
	        </div><div class="gap-patch">
	          <div class="circle"></div>
	        </div><div class="circle-clipper right">
	          <div class="circle"></div>
	        </div>
	      </div>

	      <div class="spinner-layer spinner-yellow">
	        <div class="circle-clipper left">
	          <div class="circle"></div>
	        </div><div class="gap-patch">
	          <div class="circle"></div>
	        </div><div class="circle-clipper right">
	          <div class="circle"></div>
	        </div>
	      </div>

	      <div class="spinner-layer spinner-green">
	        <div class="circle-clipper left">
	          <div class="circle"></div>
	        </div><div class="gap-patch">
	          <div class="circle"></div>
	        </div><div class="circle-clipper right">
	          <div class="circle"></div>
	        </div>
	      </div>
	</div>
</div>



<?php 
		
	}
	else 
		header("location: index.php");
?>

<script type="text/javascript">
	function redirect() {
		window.location="reserve.php";
	}
	setTimeout('redirect()',1000);
</script>


</body>
</html>