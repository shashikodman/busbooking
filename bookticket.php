<?php 
	include('session1.php');
	$selectedbus = $_SESSION['selectedbus'];

	if(isset($_GET['submit'.$selectedbus])) {
		if(isset($_GET['seat'.$selectedbus]) && isset($_GET['boarding'.$selectedbus]) && isset($_GET['dropping'.$selectedbus])) {

			include('header.php');
			include('db.php');
			echo "
		    <div class=\"row white z-depth-1\" id=\"profileheader\">
		      <h5 class=\"blue-text\" id=\"welcome\">
		        Welcome : &nbsp; $login_session1
		      </h5>
		    </div>";

			$query = "SELECT * FROM passenger WHERE username='".$_SESSION['login_user1']."'";
			$result = mysqli_query($con, $query)
				or die("Error querying database: ".mysqli_error($con));
			$row = mysqli_fetch_array($result);

			$name = $row['name'];
			$passengerid = $row['passengerid'];
			$busid = $_GET['busid'.$selectedbus];
			$tripid = $_GET['tripid'.$selectedbus];
			$date = $_GET['date'.$selectedbus];
			$to = $_GET['to'.$selectedbus];
			$from = $_GET['from'.$selectedbus];
			$price = $_GET['price'.$selectedbus];

			$query2 = "SELECT * FROM boardingpoints, droppingpoints WHERE boardingid={$_GET['boarding'.$selectedbus]} AND droppingid={$_GET['dropping'.$selectedbus]} AND boardingpoints.tripid=droppingpoints.tripid";
			$result2 = mysqli_query($con, $query2)
				or die("Error querying: ".mysqli_error($con));
			$row2 = mysqli_fetch_array($result2);

			$boardingpoint = $row2['boardingplace'];
			$droppingpoint = $row2['droppingplace'];

			$query1 = "SELECT busname FROM bus WHERE busid='$busid'";
			$result1 = mysqli_query($con, $query1)
				or die("Error querying database: ".mysqli_error($con));
			$row1 = mysqli_fetch_array($result1);

			$busname = $row1['busname'];

			$query2 = "SELECT * FROM trip WHERE busid='$busid' AND source='$from' AND destination='$to' AND date='$date'";
			$result2 = mysqli_query($con, $query2)
				or die("Error querying database: ".mysqli_error($con));
			$row1 = mysqli_fetch_array($result2);

			echo "
			  <div style=\"padding: 40px;\">
				<form  method=\"post\" action=\"reserve.php\">
					<div class=\"row\"
						<div class=\"col s6\">
							<div class=\"row\">
								<h5 class=\"blue-text\" style=\"font-size: 25px;\">Please confirm you booking</h5>
								<hr>
							
								<p><b>Passenger Name:</b> $name</p>
							
							
								<p><b>Bus Name:</b> $busname</p>

								<p><b>From:</b> $boardingpoint, $from</p>

								<p><b>To:</b> $droppingpoint, $to</p>

								<p><b>Date:</b> $date</p>

								<p><b>Seats:</b> ";

									$i = 0;

									$totalprice = 0;
									foreach ($_GET['seat'.$selectedbus] as $seat) {
										$totalprice += $price;
										$selectedseat[$i++] = $seat;			
										echo "
										$seat &nbsp; ";

									}
									$_SESSION['tripid'] = $tripid;
									$_SESSION['busid'] = $busid;
									$_SESSION['passengerid'] = $passengerid;
									$_SESSION['selectedseat'] = $selectedseat;
									$_SESSION['totalprice'] = $totalprice;	
									$_SESSION['price'] = $price;
									$_SESSION['boardingpoint'] = $boardingpoint;
									$_SESSION['droppingpoint'] = $droppingpoint;
								
								echo "					
								</p>
								<p><b>Price: $totalprice</b></p>
								<br>
								
								<a href=\"payment.php\" class=\"btn black\">
                                    Confirm & Pay
                                </a>
                               </div> 
						</div>
					</div>
				</form>
			  </div>	";

			
		}
		else
			header("location: index.php");
	}
	else
		header("location: index.php");

?>

<?php  
	include('footer.php');
?>