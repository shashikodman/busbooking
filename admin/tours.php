<?php  
	include('session.php');
	include('header.php');
?>

<div class="row red lighten-1 z-depth-1" id="profileheader">
	<h5 class="white-text" id="welcome">
		Welcome : &nbsp; <?php echo $login_session; ?>
	</h5>
</div>

<div class="row">
	<div class="col s12">
		<ul class="tabs tabs-fixed-width">
			<li class="tab col s3"><a href="#viewtrips">View Trips</a></li>
			<li class="tab col s3"><a href="#addtrip">Add Trip</a></li>
		</ul>
	</div>
	<div id="viewtrips" class="">
		
		<div class="row z-depth-2">
			<div class="container" style="font-size: 15px; font-weight: 500;">
				<div class="col s12">
					<p>Scheduled Trips</p>
				</div>
			</div>
		</div>

		<?php  
			include('db.php');
			$query = "SELECT * FROM trip, bus WHERE trip.busid=bus.busid";
			$result = mysqli_query($con, $query)
				or die("Error querying database: ".mysqli_error($con));
			if(mysqli_num_rows($result) > 0) {

				while($row = mysqli_fetch_array($result)) {

					$tripid = $row['tripid'];
					$busid = $row['busid'];
					$busname = $row['busname'];
					$source = $row['source'];
					$destination = $row['destination'];
					$date = $row['date'];
					$departure = $row['departure'];
					$arrival = $row['arrival'];
					$fare = $row['fare'];

					echo "
					<div class=\"row buslist\">
						<div class=\"container\">
							<div class=\"col s3\">
								<p>Bus ID: $busid</p>
								<p>Tripd ID: $tripid</p>
								<p>Date: $date</p>
							</div>
							<div class=\"col s4\">
								<p>Bus Name: $busname
								<p>Source: $source</p>
								<p>Destination: $destination</p>
							</div>
							<div class=\"col s3\">
								<p>Departue: $departure</p>
								<p>Arrival: $arrival</p>
								<p>Fare: $fare</p>
							</div>
							<div class=\"col s2\">
								<form method=\"post\" action=\"removetour.php\">
									<input type=\"hidden\" name=\"tripid\" id=\"tripid\" value=\"$tripid\">
									<button type=\"submit\" name=\"submit\" class=\"btn waves-effect red\" style=\"margin-top: 17px;\">Delete</button>
								</form>
							</div>
						</div>
					</div>";
				}

			} else {
				echo "<div class=\"row center-align\">
                    <p>No trips scheduled</p>
                </div>";
			}
			mysqli_close($con);
		?>

	</div>
	<div id="addtrip" class="col s12">
		<div class="container">
			<div class="col s12 m10 l6 offset-m1 offset-l3 z-depth-2 busform" style="padding: 40px;">
				<form method="get" action="addboarding.php">
					<div class="row">
						<h4>Enter trip details</h4>
						<hr>
					</div>
					<div class="row input-field">
						<input type="text" name="busid" id="busid" required>
						<label for="busid">Bus ID</label>
					</div>
					<div class="row input-field">
						<input type="text" name="source" id="source" required>
						<label for="source">Source City</label>
					</div>
					<div class="row input-field">
						<input type="text" name="destination" id="destination" required>
						<label for="destination">Destination City</label>
					</div>
					<div class="row">
						<label for="date">Date</label>
						<input type="date" name="date" id="date" required>
					</div>
					<div class="row">
						<label for="departure">Departure Time</label>
						<input type="time" name="departure" id="departure" required>
					</div>
					<div class="row">
						<label for="arrival">Arrival Time</label>
						<input type="time" name="arrival" id="arrival" required>
					</div>
					<div class="row input-field">
						<input type="text" name="fare" id="fare" required>
						<label for="fare">Fare</label>
					</div>
					<div class="row">
						<button type="Submit" name="submit" class="btn red -effect">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<?php  
	include('footer.php');
?>