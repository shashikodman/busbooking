<footer class="page-footer black">
      <div class="footer-copyright grey darken-3">
        <div class="container">
          © 2016 Copyright Text
          <!--<a class="grey-text text-lighten-4 right" href="#!">More Link</a>-->
        </div>
      </div>
    </footer>

  </div>

  <script type="text/javascript">
    
      document.getElementById("progress").style.display="none"; 
      document.getElementById("webcontent").style.display="block";  

  </script>

  <!--Import jQuery before materialize.js-->
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script type="text/javascript" src="js/materialize.min.js"></script>
  <script type="text/javascript">
      //For datepicker
    $('.datepicker').pickadate({
      selectMonths: true, // Creates a dropdown to control month
      selectYears: 15, // Creates a dropdown of 15 years to control year
      format: 'yyyy-mm-dd'
    });

    function toggle() {
      var from = document.getElementById("from").value;
      var to = document.getElementById("to").value;
      document.getElementById("from").value = to;
      document.getElementById("to").value = from;
    }

    $(document).ready(function() {
      $('select').material_select();
    });


    // Initialize collapse button
    $(".button-collapse").sideNav();
    // Initialize collapsible (uncomment the line below if you use the dropdown variation)
    //$('.collapsible').collapsible();  

    var i=2;
    $(document).ready(function(){
      $("#addboard").click(function(){
        $("#inputs").append("<div class=\"inputfields\"><h5>Boarding Point"+i+"</h5><div class=\"row\"><label for=\"boardtime"+i+"\">Time</label><input type=\"time\" name=\"boardtime[]\" id=\"boardtime"+i+"\" required></div><div class=\"row\"><label for=\"boardplace"+i+"\">Place</label><input type=\"text\" name=\"boardplace[]\" id=\"boardplace"+i+"\" required></div></div>");

          i++;
      });
    });    

    var j=2;
    $(document).ready(function(){
      $("#adddrop").click(function(){
        $("#inputs").append("<div class=\"inputfields\"><h5>Dropping Point"+j+"</h5><div class=\"row\"><label for=\"droptime"+j+"\">Time</label><input type=\"time\" name=\"droptime[]\" id=\"droptime"+j+"\" required></div><div class=\"row\"><label for=\"dropplace"+j+"\">Place</label><input type=\"text\" name=\"dropplace[]\" id=\"dropplace"+j+"\" required></div></div>");

          i++;
      });
    });     
            
  </script>
</body>
</html>