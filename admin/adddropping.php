<?php 
	include('session.php');
	include('db.php');
	if(isset($_GET['submit'])) {
		$i = 0;
		$tripid = $_GET['tripid'];
		$boardplaces = $_GET['boardplace'];
		$boardtimes = $_GET['boardtime'];
		foreach ($boardplaces as $boardplace) {
			$i = array_search($boardplace, $boardplaces);
			
			$query = "INSERT INTO boardingpoints(tripid, boardingtime, boardingplace) VALUES ('$tripid', '$boardtimes[$i]', '$boardplace')";
			$result = mysqli_query($con, $query)
				or die("Error querying: ".mysqli_error($con));
		}
	}
	include("header.php");	
?>		
<div class="container">
	<form id="droppingform" method="get" action="addtrip.php">
		<div id="inputs">
			<div class="inputfields">
				<h5>Dropping Point 1</h5>	
				<div class="row">
					<label for="droptime1">Time</label>
					<input type="time" name="droptime[]" id="droptime1" required>
				</div>
				<div class="row">
					<label for="dropplace1">Place</label>
					<input type="text" name="dropplace[]" id="dropplace1" required>
				</div>
			</div>
		</div>	
		<input type="hidden" name="tripid" <?php echo "value='$tripid'"; ?>>
		<div class="row" style="margin-top: 50px;">
			<a id="adddrop" class="btn black">Add Another Dropping Point</a>
			<button type="submit" name="submit" class="btn red">Submit</button>
		</div>
	</form>
	
</div>

<script>
	
</script>
<?php 
	include("footer.php");	
?>