<?php  
	include('session.php');
	include('header.php');
?>

<div class="row red lighten-1 z-depth-1" id="profileheader">
	<h5 class="white-text" id="welcome">
		Welcome : &nbsp; <?php echo $login_session; ?>
	</h5>
</div>

<div class="row">
	<div class="col s12">
		<ul class="tabs tabs-fixed-width">
			<li class="tab col s3"><a href="#viewbuses">View Buses</a></li>
			<li class="tab col s3"><a href="#addbus">Add Bus</a></li>
		</ul>
	</div>
	<div id="viewbuses" class="">
		
		<div class="row z-depth-2">
			<div class="container" style="font-size: 15px; font-weight: 500;">
				<div class="col s3">
					<p>Bus ID</p>
				</div>
				<div class="col s4">
					<p>Bus Name</p>
				</div>
				<div class="col s3">
					<p>Total Seat</p>
				</div>
			</div>
		</div>

		<?php  
			include('db.php');
			$query = "SELECT * FROM bus";
			$result = mysqli_query($con, $query)
				or die("Error querying database: ".mysqli_error($con));
			if(mysqli_num_rows($result) > 0) {

				while($row = mysqli_fetch_array($result)) {

					$busid = $row['busid'];
					$busname = $row['busname'];
					$totalseat = $row['totalseat'];
					$bustype = $row['bustype'];

					echo "
					<div class=\"row buslist\">
						<div class=\"container\">
							<div class=\"col s1\">
								<p>$busid</p>
							</div>
							<div class=\"col s4\">
								<p>$busname</p>
							</div>
							<div class=\"col s3\">
								<p>$bustype</p>
							</div>
							<div class=\"col s2\">
								<p>$totalseat</p>
							</div>
							<div class=\"col s2\">
								<form method=\"post\" action=\"removebus.php\">
									<input type=\"hidden\" name=\"busid\" id=\"busid\" value=\"$busid\">
									<button type=\"submit\" name=\"submit\" class=\"btn waves-effect red\" style=\"margin-top: 5px;\">Remove</button>
								</form>
							</div>
						</div>
					</div>";
				}

			} else {
				echo "<div class=\"row center-align\">
                    <p>No buses available</p>
                </div>";
			}
			mysqli_close($con);
		?>

	</div>
	<div id="addbus" class="col s12">
		<div class="container">
			<div class="col s8 offset-s2 z-depth-2 busform" style="padding: 40px;">
				<form method="post" action="addbus.php">
					<div class="row">
						<h4>Enter bus details</h4>
						<hr>
					</div>
					<div class="row input-field">
						<input type="text" name="busname" id="busname" required>
						<label for="busname">Bus Name</label>
					</div>
					<div class="row input-field">
						<select name="bustype">
					      <option value="" disabled selected>Choose Bus Type</option>
					      <option value="AC/Seater">AC/Seater</option>
					      <option value="Non-AC/Seater">Non-AC/Seater</option>
					      <option value="AC/Sleeper">AC/Sleeper</option>
					      <option value="Non-AC/Sleeper">Non-AC/Sleeper</option>
					    </select>
					    <label>Bus Type</label>
					</div>
					<div class="row">
						<button type="Submit" name="submit" class="btn red -effect">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<?php  
	include('footer.php');
?>