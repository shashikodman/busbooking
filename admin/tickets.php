<?php  
	include('session.php');
	include('header.php');
?>
<input type="search" class="light-table-filter" data-table="order-table" placeholder="Filter table data" style="padding-left: 10px;">
<table class="striped order-table table">
	<thead>
		<tr>
			<th>Passenger Name</th>
			<th>Bus Name (ID)</th>
			<th>Ticket No.</th>
			<th>Source</th>
			<th>Destination</th>
			<th>Date</th>
			<th>Seat No.</th>
		</tr>
	</thead>
	<tbody>
<?php

	$query = "SELECT * FROM bus B, passenger P, reserved R, trip T 
			  WHERE B.busid=R.busid
			  AND T.tripid=R.tripid
			  AND P.passengerid=R.passengerid";
	$result = mysqli_query($con, $query)
				or die("Error querying database: ".mysqli_error($con));	
	if(mysqli_num_rows($result) > 0) {	
		while ($row = mysqli_fetch_array($result)) {

			$from = $row['source'];
			$to = $row['destination'];
			$date = $row['date'];
			$busname = $row['busname'];
			$busid = $row['busid'];
			$passengername = $row['name'];
			$seat = $row['seat'];
			$arrival = $row['arrival'];
			$departure = $row['departure'];
			$reserveid = $row['reserveid'];

			echo "
				<tr>
					<td>$passengername</td>
					<td>$busname ($busid)</td>
					<td>$reserveid</td>
					<td>$from</td>
					<td>$to</td>
					<td>$date</td>
					<td>$seat</td>
					<td>
						<form method=\"post\" action=\"removeticket.php\">
							<input type=\"hidden\" name=\"reserveid\" id=\"reserveid\" value=\"$reserveid\"> 
							<button type=\"submit\" name=\"submit\" class=\" btn red right-align\" style=\"padding: 0 8px;\"><i class=\"material-icons\">delete<i></button>
						</form>	
					</td>
				</tr>";
		}
	}			
?>		
	</tbody>	
</table>

<script type="text/javascript">
	(function(document) {
	'use strict';

	var LightTableFilter = (function(Arr) {

		var _input;

		function _onInputEvent(e) {
			_input = e.target;
			var tables = document.getElementsByClassName(_input.getAttribute('data-table'));
			Arr.forEach.call(tables, function(table) {
				Arr.forEach.call(table.tBodies, function(tbody) {
					Arr.forEach.call(tbody.rows, _filter);
				});
			});
		}

		function _filter(row) {
			var text = row.textContent.toLowerCase(), val = _input.value.toLowerCase();
			row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
		}

		return {
			init: function() {
				var inputs = document.getElementsByClassName('light-table-filter');
				Arr.forEach.call(inputs, function(input) {
					input.oninput = _onInputEvent;
				});
			}
		};
	})(Array.prototype);

	document.addEventListener('readystatechange', function() {
		if (document.readyState === 'complete') {
			LightTableFilter.init();
		}
	});

})(document);
</script>
<?php  
	include('footer.php');
?>