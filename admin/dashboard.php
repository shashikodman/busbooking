<?php  
	include('session.php');
	include('header.php');
?>

<div style="min-height: 580px;">
	<div class="row red lighten-1 z-depth-1" id="profileheader">
		<h5 class="white-text" id="welcome">
			Welcome : &nbsp; <?php echo $login_session; ?>
		</h5>
	</div>
	<div class="row">
		<div class="container center-align" style="padding-top: 40px;">
			<div class="row">
				<h5>What would you like to do?</h5>
			</div>
			<div class="row">
				<a href="buses.php" class="btn red waves-effect col s6 offset-s3">View Buses</a>
			</div>
			<div class="row">
				<a href="tours.php" class="btn red waves-effect col s6 offset-s3">View Trips</a>
			</div>
			<div class="row">
				<a href="passengers.php" class="btn red waves-effect col s6 offset-s3">View Passengers</a>
			</div>	
			<div class="row">
				<a href="tickets.php" class="btn red waves-effect col s6 offset-s3">View Bookings</a>
			</div>
			<div class="row">
				<a href="incomes.php" class="btn red waves-effect col s6 offset-s3">View Incomes</a>
			</div>	
		</div>
	</div>
</div>

<?php  
	include('footer.php');
?>