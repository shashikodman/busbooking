<?php 
	include('login.php');
	if(isset($_SESSION['login_user'])) {
		header("location: dashboard.php");
	}

	include('header.php');
?>

<div class="container">
	
		<div class="row formcontainer">
			<div class="col s12 m6 offset-m3 z-depth-2" id="loginbox">
				<form method="post" action="">
					<div class="row" style="border-bottom: 1px solid;">
						<h4 class="center-align">Admin Login</h4>
					</div>
					<div class="row valign-wrapper">
						<div class="circle  grey lighten-1 valign z-depth-2">
							<p class="center-align">
								<i class="material-icons large white-text">perm_identity</i>
							</p>
						</div>
					</div>
					<div class="row input-field">
						<input type="text"  name="username" id="username">
						<label for="username">Username</label>
					</div>
					<div class="row  input-field">
						<input type="password" name="password" id="password">
						<label for="password">Password</label>
					</div>
					<div class="row">
						<button type="submit" class="btn waves-effect red" name="submit" style="width: 100%;">Login</button>
					</div>
					<span class="red-text"><?php echo $error; ?></span>
				</form>
			</div>
		</div>

	
</div>

<?php  
	include('footer.php');
?>