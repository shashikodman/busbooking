<!DOCTYPE html>
<html>
<head>
  <title>BusBooking-Admin</title>
  <!--Import Google Icon Font-->
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection"/>
  <link rel="stylesheet" type="text/css" href="css/style.css">
  
  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <script type="text/javascript" src="js/script.js"></script>
   
</head>

<body>

  <nav>
    <div class="nav-wrapper black">
      <a href="index.php" class="brand-logo left">Bus Booking (Admin)</a>
      <ul class="right hide-on-med-and-down">
        <li <?php if(basename($_SERVER['PHP_SELF']) == 'index.php' || basename($_SERVER['PHP_SELF']) == '' || basename($_SERVER['PHP_SELF']) == 'dashboard.php') echo 'class="active red darken-1"' ?>>
          <a href="index.php">Home</a>
        </li>
        <li <?php if(basename($_SERVER['PHP_SELF']) == 'buses.php') echo 'class="active red darken-1"' ?>>
          <a href="buses.php">Buses</a>
        </li>
        <li <?php if(basename($_SERVER['PHP_SELF']) == 'tours.php') echo 'class="active red darken-1"' ?>>
          <a href="tours.php">Tours</a>
        </li>
        <li <?php if(basename($_SERVER['PHP_SELF']) == 'passengers.php') echo 'class="active red darken-1"' ?>>
          <a href="passengers.php">Passengers</a>
        </li>
        <li <?php if(basename($_SERVER['PHP_SELF']) == 'tickets.php') echo 'class="active red darken-1"' ?>>
          <a href="tickets.php">Bookings</a>
        </li>
        <?php 
          
          if(isset($_SESSION['login_user'])) {
            echo "
            <li><a href=\"logout.php\" class=\"btn grey darken-4\">Logout</a></li>";
          } else {
            echo "
            <li><a href=\"index.php\" class=\"btn grey darken-4\">Login</a></li>";
          }
        ?>  
      </ul>

      <ul id="slide-out" class="side-nav">
        <li>
          <div class="userView">
            <div class="background black darken-4" style="border-bottom: 5px solid #e53935 ; border-right: 1px solid #e53935 ">

            </div>
            <a href="index.php" style="padding-bottom: 20px;"><h4 class="white-text">Bus Booking</h4></a>
          </div>
        </li>
        <li <?php if(basename($_SERVER['PHP_SELF']) == 'index.php' || basename($_SERVER['PHP_SELF']) == '' || basename($_SERVER['PHP_SELF']) == 'dashboard.php') echo 'class="active red darken-1"' ?>>
          <a href="index.php">Home</a>
        </li>
        <li <?php if(basename($_SERVER['PHP_SELF']) == 'buses.php') echo 'class="active red darken-1"' ?>>
          <a href="buses.php">Buses</a>
        </li>
        <li <?php if(basename($_SERVER['PHP_SELF']) == 'tours.php') echo 'class="active red darken-1"' ?>>
          <a href="tours.php">Tours</a>
        </li>
        <li <?php if(basename($_SERVER['PHP_SELF']) == 'passengers.php') echo 'class="active red darken-1"' ?>>
          <a href="passengers.php">Passengers</a>
        </li>
        <li <?php if(basename($_SERVER['PHP_SELF']) == 'tickets.php') echo 'class="active red darken-1"' ?>>
          <a href="tickets.php">Bookings</a>
        </li>
        <?php 
          
          if(isset($_SESSION['login_user'])) {
            echo "
            <li><a href=\"logout.php\" class=\"grey\">Logout</a></li>";
          } else {
            echo "
            <li><a href=\"index.php\" class=\"grey\">Login</a></li>";
          }
        ?>  
      </ul>
      <a href="#" data-activates="slide-out" class="button-collapse right hide-on-large-only"><i class="material-icons">menu</i></a>
    </div>
  </nav>

  <div class="preloader-wrapper big active" id="progress">
    <div class="spinner-layer spinner-blue-only">
      <div class="circle-clipper left">
        <div class="circle"></div>
      </div><div class="gap-patch">
        <div class="circle"></div>
      </div><div class="circle-clipper right">
        <div class="circle"></div>
      </div>
    </div>
  </div>

  <div id="webcontent">