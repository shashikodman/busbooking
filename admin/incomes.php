<?php  
	include('session.php');
	include('header.php');
?>
<div class="row" style="margin-top: 30px;">
	<h4 class="center-align">View Income</h4>
	<hr>
</div>

<div class="row">
	<div class="col s12 m6 l6 " style="padding: 0 20px;">
		<form method="get" action="incomes.php" style="margin-bottom: 50px;">
			<div class="row">
				
					<label>Start date:</label>
					<input type="date" name="start" required>
			
			</div>
			<div class="row">
				
					<label>End date:</label>
					<input type="date" name="end" required>
				
			</div>
			<div class="row">
				
					<button type="submit" name="submit" class="btn black">Submit</button>
				
			</div>

		</form>
	</div>

	<div class="col s12 m6 l6 " style="padding: 30px 20px 0 30px;">
		<?php 
			if(isset($_GET) && isset($_GET['start']) && isset($_GET['end'])) {
				$query = "SELECT SUM(price) as sum FROM reserved WHERE reservationdate>='".$_GET['start']."' AND reservationdate<='".$_GET['end']."' ";
				$result = mysqli_query($con, $query)
					or die("Error querying: ".mysqli_error($con));
				if(mysqli_num_rows($result) == 0){
					$income = 0;
				} 
				else {
					$row = mysqli_fetch_array($result);
					$income = $row['sum'];
				}	
				echo "<p style='font-size: 20px;'>Your income From: {$_GET['start']} To: {$_GET['end']} is: </p><h4>INR $income</h4>"	;	
			}
		?>
	</div>

</div>	

<?php  
	include('footer.php');
?>