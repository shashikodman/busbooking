<?php  
	include('session.php');
	include('header.php');
?>

<div class="row red lighten-1 z-depth-1" id="profileheader">
	<h5 class="white-text" id="welcome">
		Welcome : &nbsp; <?php echo $login_session; ?>
	</h5>
</div>


		
		<div class="row z-depth-2">
			<div class="" style="font-size: 15px; font-weight: 500;">
				<div class="col s1">
					<p>ID</p>
				</div>
				<div class="col s2">
					<p>Name</p>
				</div>
				<div class="col s4">
					<p>Email</p>
				</div>
				<div class="col s2">
					<p>Mobile</p>
				</div>
				
			</div>
		</div>

		<?php  
			include('db.php');
			$query = "SELECT * FROM passenger";
			$result = mysqli_query($con, $query)
				or die("Error querying database: ".mysqli_error($con));
			if(mysqli_num_rows($result) > 0) {

				while($row = mysqli_fetch_array($result)) {

					$passengerid = $row['passengerid'];
					$name = $row['name'];
					$email = $row['email'];
					$mobile = $row['mobile'];
					$username = $row['username'];

					echo "
					<div class=\"row buslist\">
						
							<div class=\"col s1\">
								<p>$passengerid</p>
							</div>
							<div class=\"col s2\">
								<p>$name</p>
							</div>
							<div class=\"col s4\">
								<p>$email</p>
							</div>
							<div class=\"col s2\">
								<p>$mobile</p>
							</div>
							<div class=\"col s2\">
								<form method=\"post\" action=\"removepassenger.php\">
									<input type=\"hidden\" name=\"passengerid\" id=\"passengerid\" value=\"$passengerid\">
									<button type=\"submit\" name=\"submit\" class=\"btn waves-effect red\" style=\"margin-top: 5px;\">Remove</button>
								</form>
							</div>
						
					</div>";
				}

			} else {
				echo "<div class=\"row center-align\">
                    <p>No buses available</p>
                </div>";
			}
			mysqli_close($con);
		?>

	</div>
	
<?php  
	include('footer.php');
?>