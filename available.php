<?php
  include('session1.php');
  if(!isset($_GET['submit'])) {
    header("location: index.php");
  }  

  include('header.php');
  include('db.php');
  echo "
    <div class=\"row white\" id=\"profileheader\">
      <h5 class=\"blue-text\" id=\"welcome\">
        Welcome : &nbsp; $login_session1
      </h5>
    </div>";

?>

<div>

<?php
  if(!empty($_GET['from']) && !empty($_GET['to']) && !empty($_GET['date'])) {
    $from = $_GET['from'];
    $to = $_GET['to'];
    $date = $_GET['date'];
         
    echo "
      <div>
        <div class=\"row blue lighten-1 z-depth-1\" id=\"tripheader\">
          <div class=\"container\">
            <div class=\"col s6\">
              <p class=\"white-text\" style=\"font-weight: 300; font-size: 15px;\">
        
                <span>$from</span> <span style=\"padding: 0 10px;\">--></span> <span>$to</span>
           
              </p>  
            </div>

            <div class=\"col s6\">
              <p class=\"white-text\" style=\"font-weight: 300; font-size: 15px;\">$date</p>
            </div> 
          </div>
        </div>";

        
        $query = "SELECT * FROM bus, trip
                  WHERE bus.busid = trip.busid
                  AND source = '$from'
                  AND destination = '$to'
                  AND date = '$date'";

        $result = mysqli_query($con, $query)
          or die("Error querying database: " . mysqli_error($con));

        if(mysqli_num_rows($result) > 0) {  

          echo "
          <ul class=\"collapsible\" data-collapsible=\"accordion\" id=\"buslist\">";

          while($row = mysqli_fetch_array($result)) {   

              $busid = $row['busid'];
              $busname = $row['busname'];
              $departure = $row['departure'];
              $arrival = $row['arrival'];
              $seats = $row['totalseat'];
              $price = $row['fare'];         
              $tripid = $row['tripid'];
              $bustype = $row['bustype'];

              $query2 = "SELECT busid, count(seat) AS seatcount
                          FROM reserved 
                          WHERE busid = $busid";

              $result2 = mysqli_query($con,$query2)
                or die("Error querying database: " . mysqli_error());
              $row2 = mysqli_fetch_array($result2);
              
              $reservedseats = $row2['seatcount'];
              $availableseats = $seats - $reservedseats;    


            echo "
              <li style=\"margin-bottom: 5px;\">
                <div class=\"collapsible-header\" onclick=\"setsession($busid)\">
                  <div class=\"row container\" style=\"margin-bottom: 0px;\">
                    <div>
                      <div class=\"col s12 m4\">
                        <div class=\"row\" id='busname'>
                          $busname
                        </div>
                        <div class=\"row\" style=\"margin-bottom: 0; margin-top: -20px;\">
                          <small class='grey-text'>$bustype</small>
                        </div>  
                      </div>
                      <div class=\"col s12 m4\">
                      <div class=\"row\" id='time'>
                          $departure &nbsp; <span class=\"material-icons\" style=\"margin-top: 3px; transform: rotate(45deg); font-size: 1rem;\">call_made</span> &nbsp; $arrival
                        </div>  
                      </div>
                      <div class=\"col s12 m2\">
                        <div class=\"row\" id='seats'>
                          $availableseats Seats
                        </div>
                      </div>
                      <div class=\"col s12 m2 right-align\">
                        <div class=\"row\" id='price'>
                          INR $price
                        </div>  
                      </div>
                    </div>
                  </div>  
                </div>

                <div class=\"collapsible-body\">
                  
                  <div class=\"container\">
                              <div class=\"row\">
                                <form method=\"get\" id=\"seatform\" action=\"bookticket.php\">
                                  <div class=\"row\">
                                    <div class=\"col s12 m6\">  
                                      <div class=\"plane\">";
                                       
                                      for($i=0, $k=0; $i<11; $i++) {  
                                       
                                        echo " 
                                          <ol class=\"cabin fuselage\">
                                            <li class=\"row--1\">
                                              <ol class=\"seats\" type=\"A\">";

                                              for($j=1; $j<=4; $j++) {

                                                $k = ++$k;
                                                $getseat = mysqli_query($con, "SELECT seat FROM reserved where busid='$busid' AND seat=$k")
                                                  or die("Error querying database: ".mysqli_error($con));

                                                  if(mysqli_num_rows($getseat) == 1) {
                                                    echo " 
                                                      <li class=\"seat\">
                                                        <input type=\"checkbox\" id=\"seat$busid$k\" name=\"seat".$busid."[]\" value=\"".$k."\" disabled />
                                                        <label for=\"seat$busid$k\">$k</label>
                                                      </li>";
                                                  } else {

                                                   echo " 
                                                      <li class=\"seat\">
                                                        <input type=\"checkbox\" id=\"seat$busid$k\" name=\"seat".$busid."[]\" value=\"".$k."\" />
                                                        <label for=\"seat$busid$k\">$k</label>
                                                      </li>";
                                                  }  
                                              }

                                            echo "    
                                              </ol>
                                            </li>
                                                                      
                                          </ol>";
                                      }

                                    echo "    
                                      </div> <!-- End plane -->
                                    </div> <!-- End column --> 

                                    <div class='col s12 m6' style='padding-top: 32px;'>
                                      <h5>Choose Boarding Point</h5>
                                      <hr>
                                      <div class='row' style='height: 150px; overflow-y: scroll;'>";
                                          	
                                      	$query4 = "SELECT * FROM boardingpoints WHERE tripid='$tripid'";
                                      	$result4 = mysqli_query($con, $query4)
                                      		or die("Error querying: ".mysqli_error($con));
                                      	while($row4 = mysqli_fetch_array($result4)) {	

                                      		echo "
                                      		<div style='margin-bottom: 0;'>
		                                        <input type='radio' class='with-gap' name='boarding$busid' id='boarding$busid{$row4['boardingid']}' value='{$row4['boardingid']}'  required/>
		                                        <label for='boarding$busid{$row4['boardingid']}' class='black-text'>".$row4['boardingtime']." &nbsp;&nbsp; <b>".$row4['boardingplace']."</b></label>
		                                    </div>    ";
	                                    } 
                                    
	                                  echo "  
                                      </div>
                                      <h5>Choose Dropping Point</h5>
                                      <hr>
                                      <div class='row' style='height: 150px; overflow-y: scroll;'>";
                                          	
                                      	$query3 = "SELECT * FROM droppingpoints WHERE tripid='$tripid'";
                                      	$result3 = mysqli_query($con, $query3)
                                      		or die("Error querying: ".mysqli_error($con));
                                      	while($row3 = mysqli_fetch_array($result3)) {	

                                      		echo "
                                      		<div style='margin-bottom: 0;'>
		                                        <input type='radio' class='with-gap' name='dropping$busid' id='dropping$busid{$row3['droppingid']}' value='{$row3['droppingid']}'  required/>
		                                        <label for='dropping$busid{$row3['droppingid']}' class='black-text'>".$row3['droppingtime']." &nbsp;&nbsp; <b>".$row3['droppingplace']."</b></label>
		                                    </div>    ";
	                                    }
                                    

                                        echo "
                                        
                                      </div>
                                    </div>

                                  </div>
                                  <div class=\"row\">
                                    <input type=\"hidden\" id=\"busid$busid\" name=\"busid$busid\" value=\"$busid\"  />
                                    <input type=\"hidden\" id=\"tripid$busid\" name=\"tripid$busid\" value=\"$tripid\"  />
                                    <input type=\"hidden\" id=\"date$busid\" name=\"date$busid\" value=\"$date\"  />
                                    <input type=\"hidden\" id=\"from$busid\" name=\"from$busid\" value=\"$from\"  />
                                    <input type=\"hidden\" id=\"to$busid\" name=\"to$busid\" value=\"$to\"  />
                                    <input type=\"hidden\" id=\"price$busid\" name=\"price$busid\" value=\"$price\"  />
                                    <button type=\"submit\" name=\"submit$busid\" class=\"btn black\" style=\"margin-left: 21px;\">
                                      Select
                                    </button>
                                  </div>
                                </form>
                              </div>  
                            </div>


                </div>
              </li>";


          }

          echo "
          </ul>";

        } else {
          echo "<div class=\"row center-align\">
                    <p>No buses available</p>
                </div>";
        }    
  } 
  mysqli_close($con);
?>      
<script type="text/javascript">
  
function setsession(x) {
  $.ajax({
    type: "POST",
    url: 'setsession.php',
    data: { x : x },
    success: function(data){
      
    }
  });
}

</script>
</div>
<?php
  include('footer.php');
?>  