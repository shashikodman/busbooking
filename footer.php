<footer class="page-footer black" style="margin-top: -10px;">
      <div class="footer-copyright grey darken-3">
        <div class="container">
          © 2016 Bus Booking
          <!--<a class="grey-text text-lighten-4 right" href="#!">More Link</a>-->
        </div>
      </div>
    </footer>

  </div>

  <script type="text/javascript">
    
      document.getElementById("progress").style.display="none"; 
      document.getElementById("webcontent").style.display="block";  

  </script>

  <!--Import jQuery before materialize.js-->
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script type="text/javascript" src="js/materialize.min.js"></script>
  <script type="text/javascript">
      //For datepicker
    $('.datepicker').pickadate({
      selectMonths: true, // Creates a dropdown to control month
      selectYears: 15, // Creates a dropdown of 15 years to control year
      format: 'yyyy-mm-dd'
    });

    function toggle() {
      var from = document.getElementById("from").value;
      var to = document.getElementById("to").value;
      document.getElementById("from").value = to;
      document.getElementById("to").value = from;
    }

    // Initialize collapse button
    $(".button-collapse").sideNav();
    // Initialize collapsible (uncomment the line below if you use the dropdown variation)
    //$('.collapsible').collapsible();  

    $(document).ready(function(){
    // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
    $('.modal').modal();
  });
            
  </script>
</body>
</html>