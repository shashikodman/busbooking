<?php
  include('session1.php');
  include("header.php");
  
  echo "
    <div class=\"row white z-depth-1\" id=\"profileheader\">
      <h5 class=\"blue-text\" id=\"welcome\">
        Welcome : &nbsp; $login_session1
      </h5>
    </div>";
?>
    
    <div id="content1">
      <div class="container formcontainer  hoverable">
        <div class="row">
          <h5 class="white-text center-align">Search for bus tickets</h5>
                  <hr><br><br>
          <div class="col l6 m8 s10 offset-s1 offset-m2 offset-l3">
            
            <form method="get" action="available.php" autocomplete="off">

              <div class="row">
                
                  <label for="from">From</label>
                  <input type="text" placeholder="Type departure city" name="from" id="from" list="cities" required>
                
              </div>

              <datalist id="cities">
                <option value="Mangalore" />
                <option value="Bangalore" />
                <option value="Chennai" />
                <option value="Mysore" />
                <option value="Mumbai" />
              </datalist>

              <div class="row toggle">
               
                  <a class="btn-floating white waves-effect right" id="toggle" onclick="toggle()"><i class="material-icons black-text">swap_vert</i></a>
                
              </div>

              <div class="row to">
                
                  <label for="to">To</label>
                  <input type="text" placeholder="Type destination city" name="to" id="to" list="cities" required>
                
              </div>

              <div class="row">
               
                  <label for="date">Date</label>
                  <input type="text" placeholder="yyyy-mm-dd" class="datepicker" name="date" id="date" required>
               
              </div>
              <br>
              <div class="row">
                
                  <button type="submit" name="submit" class="btn waves-effect waves-light blue darken-1 hoverable">Check Availability</button>
                
              </div>
            
            </form>
          
          </div>
        </div>
      </div>
    </div>

<?php
  include("footer.php");
?>      