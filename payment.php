<?php  
	
	include('session1.php');

	if(isset($_SESSION['totalprice'])) {
?>
	
<!DOCTYPE html>
<html>
<head>
	<title>Payment Gateway</title>
	<style type="text/css">
		.wrapper {
			margin-top: 50px;
			width: 600px;
			border: 1px solid #e0e0e0;
			margin-left: auto;
			margin-right: auto;
			-webkit-box-shadow: 0px 9px 18px 0px rgba(50, 50, 50, 0.75);
			-moz-box-shadow:    0px 9px 18px 0px rgba(50, 50, 50, 0.75);
			box-shadow:         0px 9px 18px 0px rgba(50, 50, 50, 0.75);
		} 

		#heading {
			color: white;
			background: grey ;
			padding: 20px 20px;
			margin-top: 0px;
			font-weight: 100;

		}

		.content {
			padding: 0 20px;
		}

		input {
			margin-top: 10px;
			border-radius: 5px;
			padding: 10px 10px;
		}

		button {
			color: white;
			background: #eeeeee ;
			border: 1px solid gray;
			border-radius: 5px;
			padding: 5px 15px;
			font-size: 18px;
			-webkit-box-shadow: inset -18px -17px 123px -39px rgba(0,0,0,0.75);
			-moz-box-shadow: inset -18px -17px 123px -39px rgba(0,0,0,0.75);
			box-shadow: inset -18px -17px 123px -39px rgba(0,0,0,0.75);
		}

		#cardnovalidate, #expiryvalidate, #cscvalidate {
			color: red;
		}

	</style>
</head>

<body>

	<div class="wrapper">
		<h2 id="heading">Enter you payment details</h2>
		<div class="content">
			<form method="post" name="paymentForm" action="process.php" onsubmit="return validate()">
				<p>
					<img src="images/cards.png">
				</p>
				<p>
					<label>Credit Card Number</label><br>
					<input type="text" name="cardno" id="cardno" maxlength="16" required style="width: 350px;" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
				</p>
				<p id="cardnovalidate"></p>
				<p>
					<label>Card Holder Name</label><br>
					<input type="text" name="name" id="name" maxlength="25" required style="width: 350px;">
				</p>
				
				<p>
					<label>Expiration</label><br>
					<input type="text" placeholder="mm" name="month" maxlength="2" required id="month" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
					<input type="text" placeholder="yyyy" name="year" maxlength="4" required id="year" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
				</p>
				<p id="expiryvalidate"></p>
				<p>
					<label>Card Security Code</label><br>
					<input type="text" name="csc" maxlength="3" required id="csc" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
				</p>
				<p>
					<b>Amount: </b><?php echo $_SESSION['totalprice']; ?>
				</p>
				<p id="cscvalidate"></p>
				<p>
					<button type="submit">Pay Now</button>
				</p>
			</form>	
		</div>	
	</div>

<script type="text/javascript">
	function validate() {
		var flag = 0;
		var cardno = document.getElementById("cardno").value;
		var month = document.getElementById("month").value;
		var year = document.getElementById("year").value;
		var csc = document.getElementById("csc").value;
		
		if(cardno.length != 16) {
			document.getElementById("cardnovalidate").innerHTML="Enter a valid card number!";
			flag = 1;
		}
		else
			document.getElementById("cardnovalidate").innerHTML="";
		
		if((!(month>0 && month<=12)) || (year.length!=4) || !(year>2000)) {
			document.getElementById("expiryvalidate").innerHTML= "Enter a valid date!";
			flag = 1;
		}
		else
			document.getElementById("expiryvalidate").innerHTML= "";

		if(csc.length != 3) {
			document.getElementById("cscvalidate").innerHTML="Enter a valid code!";
			flag = 1;
		}
		else 
			document.getElementById("cscvalidate").innerHTML="";

		if(flag == 1) {
			return false;
		}
	}
</script>	

</body>
</html>

<?php 
		
	}
	else 
		header("location: index.php");
?>