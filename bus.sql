-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 14, 2017 at 08:23 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bus`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `adminid` int(11) NOT NULL,
  `uname` varchar(25) NOT NULL,
  `upassword` varchar(25) DEFAULT NULL,
  `fname` varchar(25) DEFAULT NULL,
  `lname` varchar(25) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `mobile` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`adminid`, `uname`, `upassword`, `fname`, `lname`, `email`, `mobile`) VALUES
(2, 'admin', 'admin', 'Fname1', 'Lname1', 'admin@gmail.com', 12349876);

-- --------------------------------------------------------

--
-- Table structure for table `boardingpoints`
--

CREATE TABLE `boardingpoints` (
  `tripid` int(11) DEFAULT NULL,
  `boardingtime` time DEFAULT NULL,
  `boardingplace` varchar(30) DEFAULT NULL,
  `boardingid` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `boardingpoints`
--

INSERT INTO `boardingpoints` (`tripid`, `boardingtime`, `boardingplace`, `boardingid`) VALUES
(3, '22:05:00', 'Pumpwell Circle', 1),
(3, '22:10:00', 'Naguri', 2),
(3, '22:12:00', 'Padeel Junction NH-48', 3),
(3, '22:15:00', 'Parangipete NH-48', 4),
(4, '10:30:00', 'Pumpwell Circle', 9),
(4, '10:37:00', 'Padeel Junction NH-48', 10),
(9, '04:55:00', 'Place 1', 17),
(9, '05:00:00', 'Place 2', 18);

-- --------------------------------------------------------

--
-- Table structure for table `bus`
--

CREATE TABLE `bus` (
  `busid` int(11) NOT NULL,
  `busname` varchar(30) NOT NULL,
  `bustype` varchar(20) NOT NULL,
  `totalseat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bus`
--

INSERT INTO `bus` (`busid`, `busname`, `bustype`, `totalseat`) VALUES
(1, 'Durgamba Motors', 'AC/Seater', 48),
(2, 'Pragathi Tourist Corporation', 'Non-AC/Seater', 48),
(3, 'Canara Tourist', 'AC/Seater', 48),
(14, 'Arun Travels', 'Non-AC/Seater', 48),
(15, 'VRL Travels', 'Non-AC/Seater', 48),
(16, 'HTC', 'AC/Seater', 48);

-- --------------------------------------------------------

--
-- Table structure for table `droppingpoints`
--

CREATE TABLE `droppingpoints` (
  `tripid` int(11) DEFAULT NULL,
  `droppingtime` time DEFAULT NULL,
  `droppingplace` varchar(30) DEFAULT NULL,
  `droppingid` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `droppingpoints`
--

INSERT INTO `droppingpoints` (`tripid`, `droppingtime`, `droppingplace`, `droppingid`) VALUES
(3, '05:00:00', 'Nelamangala Circle', 1),
(3, '05:05:00', 'Dasarahalli Bus Stop', 2),
(3, '05:10:00', 'Jalahalli Cross', 3),
(3, '05:15:00', 'Gurugunte Palya', 4),
(3, '05:30:00', 'Mekri Circle', 5),
(4, '17:00:00', 'Jalahalli Cross', 6),
(4, '17:10:00', 'Mekri Circle', 7),
(9, '05:44:00', 'Place 3', 9),
(9, '05:46:00', 'Place 4', 10);

-- --------------------------------------------------------

--
-- Table structure for table `passenger`
--

CREATE TABLE `passenger` (
  `passengerid` int(11) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `mobile` bigint(11) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `passenger`
--

INSERT INTO `passenger` (`passengerid`, `name`, `email`, `mobile`, `username`, `password`) VALUES
(1, 'Passenger1', 'passenger1@gmail.com', 987654321, 'passenger1', 'password');

-- --------------------------------------------------------

--
-- Table structure for table `reserved`
--

CREATE TABLE `reserved` (
  `reserveid` int(11) NOT NULL,
  `tripid` int(11) DEFAULT NULL,
  `busid` int(11) DEFAULT NULL,
  `passengerid` int(11) DEFAULT NULL,
  `reservationdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `boardingpoint` varchar(30) NOT NULL,
  `droppingpoint` varchar(30) NOT NULL,
  `seat` int(11) DEFAULT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reserved`
--

INSERT INTO `reserved` (`reserveid`, `tripid`, `busid`, `passengerid`, `reservationdate`, `boardingpoint`, `droppingpoint`, `seat`, `price`) VALUES
(5, 4, 2, 1, '2017-02-09 10:56:20', 'Padeel Junction NH-48', 'Mekri Circle', 1, 600),
(6, 4, 2, 1, '2017-02-09 10:56:20', 'Padeel Junction NH-48', 'Mekri Circle', 2, 600);

-- --------------------------------------------------------

--
-- Table structure for table `trip`
--

CREATE TABLE `trip` (
  `tripid` int(11) NOT NULL,
  `busid` int(11) DEFAULT NULL,
  `source` varchar(25) DEFAULT NULL,
  `destination` varchar(25) DEFAULT NULL,
  `date` date NOT NULL,
  `departure` time DEFAULT NULL,
  `arrival` time DEFAULT NULL,
  `fare` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trip`
--

INSERT INTO `trip` (`tripid`, `busid`, `source`, `destination`, `date`, `departure`, `arrival`, `fare`) VALUES
(3, 1, 'Mangalore', 'Bangalore', '2017-02-28', '22:05:00', '05:30:00', 450),
(4, 2, 'Mangalore', 'Bangalore', '2017-02-28', '10:30:00', '17:00:00', 600),
(5, 3, 'Mangalore', 'Chennai', '2017-01-24', '14:45:00', '05:30:00', 1200),
(9, 14, 'Bangalore', 'Mumbai', '2017-02-28', '04:54:00', '16:54:00', 900);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`adminid`,`uname`);

--
-- Indexes for table `boardingpoints`
--
ALTER TABLE `boardingpoints`
  ADD PRIMARY KEY (`boardingid`),
  ADD KEY `tripid` (`tripid`);

--
-- Indexes for table `bus`
--
ALTER TABLE `bus`
  ADD PRIMARY KEY (`busid`);

--
-- Indexes for table `droppingpoints`
--
ALTER TABLE `droppingpoints`
  ADD PRIMARY KEY (`droppingid`),
  ADD KEY `tripid` (`tripid`);

--
-- Indexes for table `passenger`
--
ALTER TABLE `passenger`
  ADD PRIMARY KEY (`passengerid`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `reserved`
--
ALTER TABLE `reserved`
  ADD PRIMARY KEY (`reserveid`),
  ADD KEY `tripid` (`tripid`),
  ADD KEY `busid` (`busid`),
  ADD KEY `passengerid` (`passengerid`);

--
-- Indexes for table `trip`
--
ALTER TABLE `trip`
  ADD PRIMARY KEY (`tripid`),
  ADD KEY `busid` (`busid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `adminid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `boardingpoints`
--
ALTER TABLE `boardingpoints`
  MODIFY `boardingid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `bus`
--
ALTER TABLE `bus`
  MODIFY `busid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `droppingpoints`
--
ALTER TABLE `droppingpoints`
  MODIFY `droppingid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `passenger`
--
ALTER TABLE `passenger`
  MODIFY `passengerid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `reserved`
--
ALTER TABLE `reserved`
  MODIFY `reserveid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `trip`
--
ALTER TABLE `trip`
  MODIFY `tripid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `reserved`
--
ALTER TABLE `reserved`
  ADD CONSTRAINT `reserved_ibfk_1` FOREIGN KEY (`tripid`) REFERENCES `trip` (`tripid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reserved_ibfk_2` FOREIGN KEY (`busid`) REFERENCES `bus` (`busid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `trip`
--
ALTER TABLE `trip`
  ADD CONSTRAINT `trip_ibfk_1` FOREIGN KEY (`busid`) REFERENCES `bus` (`busid`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
